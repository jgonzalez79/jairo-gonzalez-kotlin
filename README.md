# Workshop #1
## This is a workshop for Kotlin.

Hi! This is the first **workshop** for the course.
In this repo you'll find a `kotlin` project created in `Intelli J` with some simple and funny exercises.

All these exercises have been linked with a unit test cases. (Included in the project)

### Instructions
To enjoy this workshop, be sure you have a:
> gitlab account,
> Intelli J IDEA installed,
> Internet,
> CLI,
> Git installed in your Laptop/Desktop.

You should approve all test to complete the workshop.


## Steps.

>## 1. Fork 
Fork the repository, click in the right corner. It allows you to have an exact version of the project, but in your own user account.

>## 2. Clone

Clone your new repo to your local. Use the `Clone` Button. You could use the IntelliJ button to clone or the CLI, or the VCS in your IDE.
#### Now you have in your Local repo, a copy of the workshop, connected with your online repo.

>## 3. Open `Intelli J` or the IDE you preferred

In your IDE import the project.

>## 4. **Solving the exercises**

You'll find some files in the ``src`` directory, divided in two:
### -Main
This directory contains three files named `classes`
Here you should solve the exercises proposed.

## -Test
This is the space where we can check the correct solution developed by you.
Chill out about it.


## Present your work.

1. When you complete your work, and all test were passed by you, commit/push to your ``gitlab`` account.
2. Make a merge request to my repo(@sigmotoa).

## Exercises of the Workshop
The following list is the exercises, you must solve.
#### Agility
- [ ] biggerThan
- [ ] order
- [ ] smallerThan
- [ ] palindromeNumber
- [ ] palindromeWord
- [ ] factorial
- [ ] isOdd
- [ ] isPrimeNumber
- [ ] isEven
- [ ] isPerfectNumber
- [ ] fibonacci
- [ ] timesDividedByThree
- [ ] fizzBuzz

#### Convertion
- [ ] kmToM1
- [ ] kmTom
- [ ] kmTocm
- [ ] mmTom
- [ ] milesToFoot
- [ ] yardToInch
- [ ] inchToMiles
- [ ] footToYard
- [ ] kmToInch
- [ ] mmToFoot
- [ ] yardToCm

#### Geometric

- [ ] squareArea1
- [ ] squareArea2
- [ ] squareArea3
- [ ] squareArea4
- [ ] squareArea5
- [ ] circleArea1
- [ ] circleArea2
- [ ] circleArea3
- [ ] circlePerimeter
- [ ] squarePerimeter
- [ ] sphereVolume1
- [ ] sphereVolume2
- [ ] pentagonArea
- [ ] calculateHypotenuse

Inside the file, you'll find the explanation for the exercises.



