/**
 * @author sigmotoa
 *
 * @version 1
 *
 * Convertion Exercises
 */
class Convertion {
    //convert of units of Metric System
    //Km to metters
    fun kmToM1(km: Double): Int {
        val m = km * 1000
        return m.toInt()
    }

    //Km to metters
    fun kmTom(km: Double): Double {
        return km * 1000
    }

    //Km to cm
    fun kmTocm(km: Double): Double {
        return km * 100000
    }

    //milimetters to metters
    fun mmTom(mm: Int): Double {
        var cm: Double = mm.toDouble() / 1000
        return cm
    }

    //convert of units of U.S Standard System
    //convert miles to foot
    fun milesToFoot(miles: Double): Double {
        return miles * 5280
    }

    //convert yards to inches
    fun yardToInch(yard: Int): Int {
        return yard * 36
    }

    //convert inches to miles
    fun inchToMiles(inch: Double): Double {
        return inch *  1.57828e-5
    }

    //convert foot to yards
    fun footToYard(foot: Int): Int {
        val ft = foot * 0.3333
        return ft.toInt()
    }

    //Convert units in both systems
    //convert Km to inches
    fun kmToInch(km: String?): Double {
        val kmf = km?.toDoubleOrNull()
        return (kmf!! * (1000 / 1) * (100 / 1) * (1 / 2.54))
    }

    //convert milimmeters to foots
    fun mmToFoot(mm: String?): Double {
        val mmfin = mm?.toDouble()
        return (mmfin!! * (1 / 304.8))
    }

    //convert yards to cm
    fun yardToCm(yard: String?): Double {
        val yardf = yard?.toDouble()
        return (yardf!! * (0.9144 / 1) * (100 / 1))
    }
}