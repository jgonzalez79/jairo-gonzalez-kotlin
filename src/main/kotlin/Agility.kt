/**
 * @author sigmotoa
 *
 * This class contains some popular brain games with numbers
 */


class Agility {


    //bigger than

    //Show if the first number is bigger than the second

    fun biggerThan(numA: String, numB:String):Boolean
    {
        var fin:Boolean = false
        fin = numA.toDouble() > numB.toDouble()
        return fin
    }


    //Sort from bigger the numbers an show in list

    fun order(numA: Int, numB: Int, numC: Int, numD: Int, numE: Int): List<Int?> {
        return listOf(numA, numB, numC, numD, numE).sorted()
    }

    //Look for the smaller number of the list
    fun smallerThan(list: List<Double>): Double{
        var fin:Double = 0.0
        var listao = list.sorted()
        fin = listao[0]
        return fin
    }
    //Palindrome number is called in Spanish capicúa
    //The number is palindrome

    fun palindromeNumber(numA: Int): Boolean
    {
        var fin:Boolean = false
        var (igual, aux) = Pair(0, 0)
        var texto: String = numA.toString()
        for (ind in texto.length - 1 downTo 0) {
            when { texto[ind] == texto[aux] -> igual++ }
            aux++
        }
        fin = when (igual) {
            texto.length -> true
            else -> false
        }
        return fin
    }
    //the word is palindrome?
    fun palindromeWord(word: String): Boolean
    {
        var fin:Boolean = false
        var (igual, aux) = Pair(0, 0)
        var pal: String = word.toString()
        for (ind in pal.length - 1 downTo 0) {
            when { pal[ind] == pal[aux] -> igual++ }
            aux++
        }
        fin = when (igual) {
            pal.length -> true
            else -> false
        }
        return fin
    }

    //Show the factorial number for the parameter
    fun factorial(numA: Int):Int
    {
        var factorial: Int = 1
        for (i in 1..numA) {
            // factorial = factorial * i;
            factorial *= i.toInt()
        }
        return factorial
    }

    //is the number odd
    fun is_Odd(numA: Byte): Boolean {
        return numA % 2 != 0
    }

    //is the number prime
    fun isPrimeNumber(numA:Int): Boolean
    {
        var fin:Boolean = false
        var Conteo = 0
        var Lim = 0
        (1 .. numA!!).forEach { div ->
            if(numA % div == 0) {
                Conteo++
                Lim++
                if(Lim == 10) {
                    Lim = 0
                }
            }
        }
        fin = when(Conteo) {
            2 -> true
            else -> false
        }
        return fin
    }

    //is the number even
    fun is_Even(numA: Byte): Boolean
    {
        return numA % 2 == 0
    }
    //is the number perfect
    fun isPerfectNumber(numA:Int): Boolean
    {
        var plus = 0
        for(i in 1 .. numA / 2) {
            if(numA % i == 0) {
                plus += i
            }
        }
        if(plus == numA) { return true }
        return false
    }
    //Return an array with the fibonacci sequence for the requested number
    fun fibonacci(numA: Int): List<Int?>
    {
        var fib = 0
        var aux = 1
        val fibonacci = mutableListOf(0)
        if(numA > 0) {
            (0 .. numA).forEach { fibonacci.add(fib)
                aux += fib
                fib = aux - fib
            }
        } else {
            println("El numero debe ser mayor a cero!!")
        }
        fibonacci.removeAt(0)
        return fibonacci
    }
    //how many times the number is divided by 3
    fun timesDividedByThree(numA: Int):Int
    {
        var num = numA / 3
        num = num.toInt()
        return num
    }

    //The game of fizzbuzz
    fun fizzBuzz(numA: Int):String?
    {
        var fin: String = ""
        fin = if ( numA%15 == 0) {
            "FizzBuzz"
        } else if(numA%5 == 0) {
            "Buzz"
        } else if(numA%3 == 0){
            "Fizz"
        } else {
            numA.toString()
        }
        return fin
    }

}

